/** @file */
#include <conio.h>
#include <windows.h>
#include <string>
#include <iostream>
#include <cstdio>

///////////////////////////////////////////////////////////////////////
/// \brief Input with time limit.
/// Prevent user from inserting more input after time limit has been spent.
/// \param timeLeft Time limit, in milliseconds.
/// \param maxLen Maximum input length (to prevent long input that
/// causes a new line).
/// \return Input string.
///////////////////////////////////////////////////////////////////////
std::string timedLineInput(unsigned int timeLeft, size_t maxLen = 40);

int main()
{
    std::string s = timedLineInput(11000);
    std::cout << s << "\n";
}

std::string timedLineInput(unsigned int timeLeft, size_t maxLen)
{
    std::string buff;
    size_t cursorIndex = 0;
    DWORD deadline = GetTickCount() + timeLeft;
    while (1)
    {
        DWORD now = GetTickCount();
        timeLeft = deadline < now ? 0 : deadline - now;
        if (!timeLeft) break;
        if (kbhit())
        {
            int ch = getch();
            if (ch == '\r' || ch == '\n') break;
            if (ch == '\b') //backspace
            {
                if (!buff.empty()) //there is character to delete
                {
                    buff.erase(--cursorIndex, 1);
                    printf("\b \b"); //delete last character on screen (a hack)
                }
            }
            else if (ch == 0xE0) //DEL, arrow keys, ... emits 2 characters 0xE0
            {
                ch = getch();    //and this character
                if (ch == 0x53) //DEL
                {
                    if (!buff.empty())
                    {
                        if (cursorIndex == buff.size())
                        {
                            buff.erase(--cursorIndex);
                            printf("\b \b");
                        }
                        else
                        {
                            buff.erase(cursorIndex, 1);
                        }
                    }
                }
                else if (ch == 0x4b) //left arrow
                {
                    if (cursorIndex > 0) cursorIndex--;
                }
                else if (ch == 0x4d) //right arrow
                {
                    if (cursorIndex < buff.size()) cursorIndex++;
                }
                else if (ch == 0x47) //Home
                {
                    cursorIndex = 0;
                }
                else if (ch == 0x4f) //End
                {
                    cursorIndex = buff.size();
                }
            }
            else if (buff.size() < maxLen)
            {
                buff.insert(cursorIndex++, 1, ch);
            }
        }
        printf("\r%79s", ""); //clear line
        //5 is magic number for time limit < 100s.
        //For 100s < time limit < 1000s use 6, or convert to minutes...
        printf("\r%5.2fs    %s", timeLeft / 1000.0, buff.c_str());
        //set cursor position without gotoxy...
        printf("\r%5.2fs    %s", timeLeft / 1000.0, buff.substr(0, cursorIndex).c_str());
        Sleep(5); //decrease this number until there is no blinking on screen
    }
    printf("\r%5.2fs    %s\n", timeLeft / 1000.0, buff.c_str());
    return buff;
}
