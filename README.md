# Windows Console Timed Input

Timed input on Windows console (cmd.exe)

- Return input string after Enter pressed OR current string if time limit passed
- Has time remaining indicator
- Support Backspace, Delete, Home, End, Left/Right arrow keys

## Usage
```
// Time limit 10 seconds, input max length 50 characters
std::string input = timedLineInput(10000, 50);
```

## License
MIT